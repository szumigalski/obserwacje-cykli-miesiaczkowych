const { Double } = require("bson");
const { bool, array } = require("prop-types");

export const DaySchema = {
    name: 'Day',
    properties: {
      id: 'string',
      temperature: 'double',
      isTemperatureGood: 'bool',
      szyjka: 'object',
      sluz: 'object',
      numberOfCycle: 'int'
    }
  };