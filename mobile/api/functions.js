import {DaySchema} from './models'
const Realm = require('realm');

const formatDate = () => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

export const addNewDay = () => {
    Realm.open({schema: DaySchema}).then((realm) => {
        let date = formatDate()
        let day = realm
                    .objects('Word')
                    .filtered(`id = "${date}"`);
        if (!day) {

        }
        realm.write(() => {
            realm.create('Day', {
              id: date,
              temperature: 0,
              isTemperatureGood: false,
              szyjka: {},
              sluz: {},
              numberOfCycle: 0
            });
        });
        realm.close();
      });
}