export const theme = {
    colors: {
      1: '#C4C4C4',
      2: '#656666',
      3: '#69CCB9',
      4: '#E24B84',
      5: '#D5DBDA',
      6: '#767777',
    },
  };