import React from 'react'
import {Layout, Button, Text} from '@ui-kitten/components'

const MainPage = ({ navigation }) => {
    return(
        <Layout style={{ justifyContent: 'space-around', height: '100%', padding: 30 }}>
            <Layout style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <Button
                    onPress={() => navigation.pop()}
                    >
                    <Text style={{color: 'white', fontSize: 25}}>Temperatura</Text>
                </Button>
                <Button
                    onPress={() => navigation.navigate('Temperatura')}
                    >
                    <Text style={{color: 'white', fontSize: 25}}>K</Text>
                </Button>
                <Button
                    onPress={() => navigation.navigate('Temperatura')}
                    >
                    <Text style={{color: 'white', fontSize: 25}}>P</Text>
                </Button>
            </Layout>
            {/*<Button
                onPress={() => navigation.navigate('Sluz')}
                >
                <Text style={{color: 'white', fontSize: 25}}>Śluz</Text>
            </Button>
            <Button
                onPress={() => navigation.navigate('Szyjka')}
                >
                <Text style={{color: 'white', fontSize: 25}}>Szyjka macicy</Text>
            </Button>*/}
            <Button
                onPress={() => navigation.navigate('Edycja')}
                >
                <Text style={{color: 'white', fontSize: 25}}>Edycja</Text>
            </Button>
        </Layout>)
}

export default MainPage