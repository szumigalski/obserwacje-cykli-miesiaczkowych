import React, {useState, useEffect} from 'react'
import Voice from '@react-native-community/voice';
import {Button, Icon, Input, Layout, CheckBox, Text} from '@ui-kitten/components';
import axios from 'axios'
import { address } from '../../config'
import moment from 'moment'
import { loginUser } from '../../addons'
import Toast from 'react-native-simple-toast';
import { View } from 'react-native'
import _ from 'lodash'

const Temperature = ({navigation}) => {
    let [isRecording, setIsRecording] = useState(false)
    let [temperature, setTemperature] = useState('36.6')
    let [isTemperatureGood, setIsTemperatureGood]= useState(true)
    let [date, setDate] = useState('')
    let [dates, setDates] = useState(null)
    let [isSaved, setIsSaved] = useState(false)

    useEffect(() => {
      setDate(new Date())
      Voice.onSpeechStart = onSpeechStart
      Voice.onSpeechEnd = onSpeechEnd
      Voice.onSpeechError = onSpeechError
      Voice.onSpeechResults = onSpeechResults
      getDays()
    }, [])

    const onSpeechStart = (e) => {
        console.log('onSpeechStart: ', e);
        setIsRecording(true)
      };

    const onSpeechEnd = (e) => {
        console.log('onSpeechEnd: ', e);
        setIsRecording(false)
      };

    const onSpeechError = (e) => {
        console.log('onSpeechError: ', e);
        setIsRecording(false)
      };

    const onSpeechResults = (e) => {
        console.log('onSpeechResults: ', e.value[0].toString().replace(',', '.').replace(' ', '.'));
        setTemperature(e.value[0].toString().replace(',', '.').replace(' ', '.'))
        setIsRecording(false)
      };

      const MicIcon = (props) => (
        <Icon {...props} name='mic-outline' />
      );

      const MicOffIcon = (props) => (
        <Icon {...props} name='mic-off-outline'/>
      );

      const getDays = async () => {
        let token = await loginUser()
        await axios.get(address + '/day/days', {
          headers: {
            'Authorization': `Bearer ${token}` 
          }
        }).then(res => {
          setDates(res.data)
          let hasDate = _.find(res.data, {id : moment(new Date()).format('DD-MM-YYYY')}) 
          hasDate
            ? setIsSaved(hasDate._id)
            : setIsSaved(false)
        })
        .catch(err => console.log('err: ', err))
      } 

      const save = async () => {
        let date = new Date()
        let token = await loginUser()
        console.log('xdd', address + '/day/day/'+isSaved)
        
        if(token) {
          if(isSaved) {
            await axios.put(address + '/day/day/'+isSaved, {
              temperature: temperature,
              hour: moment(date).format('HH:mm')
            },
            {
              headers: {
                'Authorization': `Bearer ${token}` 
              }
            }).then(res => {
              console.log(res)
              Toast.show('Zapisano temperaturę');
            })
            .catch(err => console.log('err', err))
          } else {
            await axios.post(address + '/day/day', {
              temperature: temperature,
              hour: moment(date).format('HH:mm')
            },
            {
              headers: {
                'Authorization': `Bearer ${token}` 
              }
            }).then(res => {
              console.log(res)
              Toast.show('Zapisano temperaturę');
            })
            .catch(err => Toast.show('Błąd zapisu'))
                        
          }
        }
      }

      return(
        <Layout style={{height: '100%', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>{moment(date.toString()).format('DD-MM-YYYY, hh:mm:ss')}</Text>
            <Text style={{ color: isSaved ? 'green' : 'red'}}>{isSaved ? 'Temperatura już dodana' : 'Brak dzisiejszej temperatury'}</Text>
            <Input
              style={{height: 100, fontSize: 20, width: '50%', marginTop: 40, marginBottom: 40}}
              size='large'
              textStyle={{ height: 70, fontSize: 50 }}
              onChangeText={setTemperature}
              value={temperature}
              />
            <Button
                onPress={() => isRecording ? Voice.stop() : Voice.start('pl-PL')}
                size='giant'
                style={{height: 100, width: 100}}
                accessoryLeft={isRecording ? MicOffIcon : MicIcon}
              />
              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <Text style={{marginRight: 40}}>Temperatura jest dokładna</Text>
                <CheckBox
                  checked={isTemperatureGood}
                  onChange={nextChecked => setIsTemperatureGood(nextChecked)}
                  />
              </View>
              <Button
                onPress={() => save()}
                style={{marginTop: 40}}
                >Zapisz</Button>
              <Button
                style={{position: 'absolute', bottom: 10, left: 10}}
                onPress={() => navigation.navigate('Strona Główna')}
                >Wróć</Button>
        </Layout>
      )
}

export default Temperature