import React, {useState} from 'react'
import { Layout, Text, CheckBox, Button } from '@ui-kitten/components'
import {sluzOptions} from './sluzOptions'

const Sluz = ({ navigation }) => {
    const [sluzCheckboxes, setSluzCheckboxes] = useState({
        wet: false,
        extensible: false,
        transparent: false,
        damp: false,
        sticky: false,
        turbid: false,
        dry: false,
        atypical: false
    })

    const setCheckbox = (isChecked, name) => {
        let newCheckboxes = { ...sluzCheckboxes }
        newCheckboxes[name] = isChecked
        setSluzCheckboxes(newCheckboxes)
    }

    return (
        <Layout style={{height: '100%'}}>
            <Text category='h2'>Śluz</Text>
            {sluzOptions.map((option, i) =>
                <Layout key={i} style={{flexDirection: 'row', justifyContent: 'space-between', padding: 20}}>
                    <Text style={{fontSize: 20}}>{option.name}</Text>
                    <CheckBox
                        checked={sluzCheckboxes[option.value]}
                        onChange={nextChecked => setCheckbox(nextChecked, option.value)}
                    />
                </Layout>
            )}
            <Button
                style={{position: 'absolute', bottom: 10, left: 10}}
                onPress={() => navigation.pop()}
                >Wróć</Button>
            <Button
                style={{position: 'absolute', bottom: 10, right: 30}}
                onPress={() => navigation.pop()}
                >Wyślij</Button>
        </Layout>)
}

export default Sluz