export const sluzOptions = [
    {
        name: 'mokro/ślisko/naol.',
        value: 'wet'
    },
    {
        name: 'rozciągliwy',
        value: 'extensible'
    },
    {
        name: 'przejrzysty',
        value: 'transparent'
    },
    {
        name: 'wilgotno',
        value: 'damp'
    },
    {
        name: 'lepki, gęsty',
        value: 'sticky'
    },
    {
        name: 'mętny',
        value: 'turbid'
    },
    {
        name: 'sucho',
        value: 'dry'
    },
    {
        name: 'nietypowy',
        value: 'atypical'
    }
]