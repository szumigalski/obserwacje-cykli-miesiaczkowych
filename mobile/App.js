import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {ThemeProvider} from 'styled-components';
import {theme} from './style/theme';
import MainPage from './src/MainPage/MainPageComponent'
import Temperature from './src/Temperature/TemperatureComponent'
import Szyjka from './src/Szyjka/SzyjkaComponent'
import Sluz from './src/Sluz/SluzComponent'
import Edit from './src/Edit/EditComponent'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = () => {

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <ThemeProvider theme={theme}>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName="Temperatura"
              screenOptions={{
                headerShown: false,
              }}>
              <Stack.Screen
                name="Strona Główna"
                component={MainPage}
              />
              <Stack.Screen
                name="Temperatura"
                component={Temperature}
              />
              <Stack.Screen
                name="Edycja"
                component={Edit}
              />
              <Stack.Screen
                name="Sluz"
                component={Sluz}
              />
              <Stack.Screen
                name="Szyjka"
                component={Szyjka}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </ThemeProvider>
      </ApplicationProvider>
    </>
  );
};

export default App;
