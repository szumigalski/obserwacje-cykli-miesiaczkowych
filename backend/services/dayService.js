const { Day } = require('../models');
const moment = require('moment');

const createDay = async (dayBody) => {
    console.log('dayBody', dayBody)
    if(!dayBody.id) {
      dayBody.id = moment().format('DD-MM-YYYY')
    }
    console.log('dayBody', dayBody)
    const day = await Day.create(dayBody);
    return day;
  };

const createDays = async (dayBody) => {
    const days = await Day.insertMany(dayBody);
    return days;
  };

const queryDays = async (filter, options) => {
    const days = await Day.find();
    return days;
  };

const getDayById = async (id) => {
    return Day.find({_id: id});
  };

const deleteDayById = async (id) => {
    return Day.findByIdAndDelete(id, function (err, docs) {
      if (err){
          console.log(err)
      }
      else{
          console.log("Deleted : ", docs);
      }
  });
  };

const deleteAllDays = async (id) => {
    return Day.deleteMany();
  };

const updateDayById = async (id, body) => {
    console.log('lol: ', id, Day.findOneAndUpdate({ _id: id}, body))
    return Day.findOneAndUpdate({ _id: id}, body)
  };

module.exports = {
    createDay,
    queryDays,
    getDayById,
    deleteDayById,
    updateDayById,
    createDays,
    deleteAllDays,
  };