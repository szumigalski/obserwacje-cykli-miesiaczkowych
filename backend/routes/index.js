const express = require('express');
const dayRoute = require('./dayRoute');
const userRoute = require('./userRoute')

const router = express.Router();

router.use('/day', dayRoute);
router.use('/auth', userRoute);

module.exports = router;