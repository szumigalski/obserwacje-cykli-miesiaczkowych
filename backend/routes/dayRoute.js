const express = require('express');
//const authController = require('../controllers/auth') 
const dayController = require('../controllers/dayController');

const router = express.Router();

router
  .get('/days', dayController.getDays)
  .get('/day/:dayId', dayController.getDay)
  .post('/day/', authController.accessTokenVerify, dayController.createDay)
  .post('/days', authController.accessTokenVerify, dayController.createDays)
  .put('/day/:dayId', authController.accessTokenVerify, dayController.updateDay)
  .delete('/days', authController.accessTokenVerify, dayController.deleteAllDays)
  .delete('/day/:dayId', authController.accessTokenVerify, dayController.deleteDay)

module.exports = router;
