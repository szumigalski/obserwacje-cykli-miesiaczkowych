const catchAsync = require('../utils/catchAsync');
const { dayService } = require('../services');
const httpStatus = require('http-status');
const pick = require('../utils/pick')
const ApiError = require('../utils/ApiError');


  const createDay = catchAsync(async (req, res) => {
    const day = await dayService.createDay(req.body);
    res.status(httpStatus.CREATED).send(day);
  });

  const createDays = catchAsync(async (req, res) => {
    console.log('create')
    const days = await dayService.createDays(req.body);
    res.status(httpStatus.CREATED).send(days);
  });

  const getDays = catchAsync(async (req, res) => {
    const filter = pick(req.query, ['name', 'role']);
    const options = pick(req.query, ['sortBy', 'limit', 'page']);
    const result = await dayService.queryDays(filter, options);
    res.send(result);
  });

  const getDay = catchAsync(async (req, res) => {
    const day = await dayService.getDayById(req.params.dayId);
    if (!day) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Day not found');
    }
    res.send(day);
  });

  const updateDay = catchAsync(async (req, res) => {
    console.log('ech', req.params.dayId, req.body)
    const day = await dayService.updateDayById(req.params.dayId, req.body);
    if (!day) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Day not found');
    }
    res.send(day);
  });

  const deleteDay = catchAsync(async (req, res) => {
    const day = await dayService.deleteDayById(req.params.dayId);
    res.send(day);
  });

  const deleteAllDays = catchAsync(async (req, res) => {
    const days = await dayService.deleteAllDays();
    res.send(days);
  });

module.exports = {
    createDay,
    getDays,
    getDay,
    updateDay,
    deleteDay,
    createDays,
    deleteAllDays,
  };