const { User } = require('../models')
const { userBackend } = require('../config.js')

async function isUsersExist() {
  const exec = await User.find().exec()
  return exec.length > 0
}

// Initialize first user
const initializeData = async () => {
  if(!await isUsersExist()) {
    const user = [
      new User(userBackend)
    ]
    let done = 0;
    for (let i = 0; i < user.length; i++) {
      user[i].save((err, result) => {
        done++;
      })
    }
  }
}

module.exports = {
    initializeData,
  };