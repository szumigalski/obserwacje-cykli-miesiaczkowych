const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose')
const routes = require('./routes');
const cors = require('cors');
const { user, password, adress, port } = require('./config.js')
const { initializeData } = require('./seed/userSeeder.js')

const corsOpts = {
  origin: '*',

  methods: [
    'GET',
    'POST',
  ],

  allowedHeaders: [
    'Content-Type',
  ],
};

app.use(cors(corsOpts));
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

const url = `mongodb://${user}:${password}@${adress}:${port}/cycleDb`;
console.log('url', url)
let mongooseOptions = {
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  autoIndex: true
}

mongoose.connect(url, mongooseOptions).then(() => {
    console.log('Connected with Mongo')
});

mongoose.connection.on('connected', () => {
  initializeData()
  console.log('Initialize user')
});

app.listen(5000, function() {
    console.log('Server started on 5000')
  })

app.use('/', routes);
app.get('/test', function (req, res) {
    res.send('hello world')
  })