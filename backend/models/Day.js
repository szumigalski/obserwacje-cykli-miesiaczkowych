const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const daySchema = mongoose.Schema(
  {
    id: {
      type: String,
      required: true,
      index: true,
      unique: true
    },
    temperature: {
        type: Number,
        default: 0
    },
    isTemperatureGood: {
        type: Boolean,
        default: true
    },
    hour: {
      type: String,
      default: '8:00'
    },
    szyjka: [],
    sluz: {
        wet: { type: Boolean, default: false }, // mokro/ślisko/naol.
        extensible: { type: Boolean, default: false }, // rozciągliwy
        transparent: { type: Boolean, default: false }, // przejrzysty
        damp: { type: Boolean, default: false }, // wilgotno
        sticky: { type: Boolean, default: false }, // lepki, gęsty
        turbid: { type: Boolean, default: false }, // mętny
        dry: { type: Boolean, default: false }, // sucho
        atypical: { type: Boolean, default: false }, // nietypowy
    },
    blood: {
        type: String,
        default: 'n'
    }
  }
);
daySchema.plugin(uniqueValidator, { message: 'Error, expected id to be unique.' })
const Day = mongoose.model('Day', daySchema);

module.exports = Day;
