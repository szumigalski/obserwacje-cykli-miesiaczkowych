import React from 'react'
import {
  ComposedChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine,
  Area
  } from 'recharts';

const CustomizedLabel = ({x, y, stroke, value, extraData, pages, index}) => {
      let newIndex = index + 1
      let change = ''
      if(newIndex > extraData[pages].firstAfterSix - 7 && newIndex < extraData[pages].firstAfterSix + 3) {
        if(newIndex >= extraData[pages].firstAfterSix) {
          change = newIndex - extraData[pages].firstAfterSix + 1
        } else {
          change = extraData[pages].firstAfterSix - newIndex
        }
      }
      return <text x={x} y={y} dy={-4} fill={stroke} fontSize={12} textAnchor="middle">{change}</text>;
  }

  const CustomizedDot = ({ cx, cy, stroke, payload, value }) => {
    let newR = 2
    let newStrokeWidth = 0.5
    if(!value) {
      newR = 0
      newStrokeWidth = 10
    }
    return <circle cx={cx} cy={cy} r={newR} stroke="black" strokeWidth={newStrokeWidth} fill="red" />
  };

const Chart = ({data, linesTemps, pages, extraData}) => {
    return(
       <ComposedChart
        width={1000}
        height={500}
        data={data[pages]}
        margin={{
          top: 5, right: 30, left: 65, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <Area type="monotone" dataKey="k" fill="#FF0000" stroke="#FF0000" />
        <Area type="monotone" dataKey="p" fill="#781f18" stroke="#781f18" />
        <XAxis dataKey="number" scale="point" />
        <YAxis
          ticks={[36, 36.05, 36.1, 36.15, 36.2, 36.25, 36.3, 36.35, 36.4, 36.45, 36.5, 36.55, 36.6, 36.65, 36.7, 36.75, 36.8, 36.85, 36.9, 36.95, 37]}
          interval={0}
          domain={[36, 37]}/>
        <ReferenceLine y={linesTemps[pages]} stroke="red" />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="temp"
          stroke="#000"
          strokeWidth={1.5}
          label={<CustomizedLabel extraData={extraData} pages={pages} />}
          dot={<CustomizedDot extraData={extraData} pages={pages} />}
          />
      </ComposedChart>
    )

}

export default Chart