import React, { useEffect, useState } from 'react'
import './App.css';
import { Segment, Button, Table } from 'semantic-ui-react'
import Chart from './components/ChartComponent/ChartComponent'
import axios from 'axios'
import DayModal from './components/DayModalComponent/DayModalComponent'
import {addDays} from './data'
import {StyledTable, StyledTr, StyledTd} from './AppStyle'
const clonedeep = require('lodash.clonedeep')

function App() {
  const [isModalOpen, setModalOpen] = useState(false)
  const [data, setData] = useState([])
  const [linesTemps, setLinesTemps] = useState([])
  const [pages, setPages] = useState(0)
  const [extraData, setExtraData] = useState([])

  let sluzOptions = ["wet",
  "extensible",
  "transparent",
  "damp",
  "sticky",
  "turbid",
  "dry",
  "atypical"]

  let sluzNames = ['mokro/ślisko/naol.', 'rozciągliwy', 'przejrzysty', 'wilgotno', 'lepki, gęsty', 'mętny', 'sucho', 'nietypowy']

  const separateCycles = (data) => {
    let listOfCycles = []
    let counter = 0
    let daysCounter = 1
    let newDay
    let sixLowerTempsList = []
    let linesTemps = []
    let stopLines = false
    let extraDataLocal = []
    let sluzObject = {
      "wet": [],
      "extensible": [],
      "transparent": [],
      "damp": [],
      "sticky": [],
      "turbid": [],
      "dry": [],
      "atypical": []
    }
    data.map((day, i) => {
      if(i === 0 || (data[i-1].blood === 'n' && day.blood !== 'n')) {
        daysCounter = 1
        newDay = { name: day.id, number: daysCounter }
        day.temperature && (newDay.temp = day.temperature)
        day.blood === 'k' && (newDay.k = 37)
        i > 0 && data[i-1].blood === 'k' && day.blood === 'p' && (newDay.k = 37)
        day.blood === 'p' && (newDay.p = 37)
        listOfCycles.push([newDay])
        extraDataLocal.push({ sluz: clonedeep(sluzObject)})
        console.log('extraDataLocal', extraDataLocal)
        Object.keys(day.sluz).map(key => {
          console.log(key, extraDataLocal[counter].sluz[key])
          extraDataLocal[counter].sluz[key].push(day.sluz[key])
        })
        i !== 0 && (counter += 1)
        stopLines = false
        sixLowerTempsList = []
      } else {
        newDay = { name: day.id, number: daysCounter }
        day.temperature && (newDay.temp = day.temperature)
        day.blood === 'k' && (newDay.k = 37)
        i > 0 && data[i-1].blood === 'k' && day.blood === 'p' && (newDay.k = 37)
        i > 0 && data[i-1].blood === 'p' && day.blood === 'n' && (extraDataLocal[counter].endBlood = daysCounter - 1)
        day.blood === 'p' && (newDay.p = 37)
        listOfCycles[counter].push(newDay)
        Object.keys(day.sluz).map(key => {
          extraDataLocal[counter].sluz[key].push(day.sluz[key])
        })
        if(day.temperature !== 0 && !stopLines) {
          if(sixLowerTempsList.length < 6) {
            sixLowerTempsList.push(day.temperature)
          } else {
            if(day.temperature > Math.max(...sixLowerTempsList)) {
              extraDataLocal[counter].firstAfterSix = daysCounter
              console.log('find', sixLowerTempsList, Math.max(...sixLowerTempsList), day.temperature)
              stopLines = true
              linesTemps.push(Math.max(...sixLowerTempsList))
            }
          }
        }
        extraDataLocal[counter].endCycle = daysCounter
      }
      daysCounter += 1
      console.log('newDay', newDay)
    })
    console.log('listOfCycles', listOfCycles)
    console.log('extraDataLocal', extraDataLocal)
    console.log('linesTemps', linesTemps)
    setExtraData(extraDataLocal)
    setLinesTemps(linesTemps)
    return listOfCycles
  }
  useEffect(() => {
    //addDays()
    axios.get('http://207.154.208.236:5000/day/days').then(function (response) {
      // handle success
      let date = response.data.sort(function(a, b){
        a = a.id.split('-');
        b = b.id.split('-');
        return a[2] - b[2] || a[1] - b[1] || a[0] - b[0];
    })
      console.log(date);
      let newData = separateCycles(date)
      setData(newData)
    })
  }, [])

  return (
    <Segment style={{ margin: 20}}>
      {data.map((d, i) => <Button onClick={() => setPages(i)}>{i}</Button>)}
      <div style={{display: 'flex', marginTop: 20}}>
        <div>
        {data && <Chart
          data={data}
          linesTemps={linesTemps}
          pages={pages}
          extraData={extraData}
          />}
          <div style={{display: 'flex'}}>
            <div style={{display: 'flex', flexDirection: 'column', width: 120}}>
              {sluzNames.map(item => <div>{item}</div>)}
            </div>
            {extraData.length &&<StyledTable style={{borderCollapse: 'collapse', border: '1px solid black'}}>
              {sluzOptions.map((option, i) =>
                <StyledTr>
                  {extraData[pages].sluz[option].map(item =>
                    <StyledTd>{item ? 'x' : ''}</StyledTd>
                  )}
                </StyledTr>
              )}
            </StyledTable>}
          </div>
        </div>
        <Table>
          <Table.Body>
            <Table.Row>
              <Table.Cell>Numer cyklu</Table.Cell>
              <Table.Cell>{pages + 1}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Ostatni dzień miesiączki</Table.Cell>
              {extraData.length && <Table.Cell>{extraData[pages].endBlood}</Table.Cell>}
            </Table.Row>
            <Table.Row>
              <Table.Cell>Długość cyklu</Table.Cell>
              {extraData.length && <Table.Cell>{extraData[pages].endCycle}</Table.Cell>}
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    </Segment>

  );
}

export default App;