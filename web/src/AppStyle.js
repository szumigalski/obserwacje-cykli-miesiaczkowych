import styled from 'styled-components'

export const StyledTable = styled.table`
    border: 1px solid black;
    width: 855px;
`

export const StyledTr = styled.tr`
    border: 1px solid black;
`

export const StyledTd = styled.td`
    border: 1px solid black;
    width: 10px;
    height: 20px;
`