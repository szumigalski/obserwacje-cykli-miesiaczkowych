# Obserwacje cyklu miesiączkowego

## Infrastruktura

Na początku potrzebujesz serwera na którym będziesz trzymał bazę danych i backend (np droplet na DigitalOcean)

### Baza danych

pobierz aktualności  
`sudo apt update`  
instalacja  
`sudo apt install -y mongodb`  
sprawdzenie statusu  
`sudo systemctl status mongodb`  
edycja konfiguracji  
`sudo nano /etc/mongodb.conf` 
po zmianach pamiętaj o restarcie  
`sudo systemctl restart mongodb`
